class UserGadgetRequestsController < ApplicationController
  def create
    @ugr = UserGadgetRequest.create(permitted_attributes)
    AdminNotificationMailer.user_gadget_request(@ugr).deliver_now if @ugr.valid?
  end

  private

  def permitted_attributes
    params.permit(:phone)
  end
end
