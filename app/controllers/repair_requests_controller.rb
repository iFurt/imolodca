class RepairRequestsController < ApplicationController
  def create
    @repair_request = RepairRequest.create(permitted_attributes)
    AdminNotificationMailer.repair_request(@repair_request).deliver_now if @repair_request.valid?
  end

  private

  def permitted_attributes
    params.permit(:user_name, :phone, :description)
  end
end
