class SubscriptionsController < ApplicationController
  def create
    @subscription = Subscription.create(permitted_attributes)
  end

  private

  def permitted_attributes
    params.permit(:name, :email)
  end
end
