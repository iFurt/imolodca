class WelcomeController < ApplicationController
  def index
    @gadgets = Gadget.all
    @masters = Master.all
    @user_gadgets = UserGadget.all
    @clients_ppl = Client.ppl
    @clients_cmp = Client.cmp
    @feedbacks = Feedback.all
    @promo = Promo.active

    @phone_numbers = ['067 598 1 333', '063 598 1 333', '066 598 1 333']
    @telegram_id = AdminIdentificator.find_by(name: 'telegram')&.value || 'ifurt'
  end

  def become_a_partner
    AdminNotificationMailer.become_a_partner(params[:name], params[:contact]).deliver_now
  end
end
