class RepairBookingsController < ApplicationController
  def create
    @repair_booking = RepairBooking.create(permitted_attributes)
    AdminNotificationMailer.repair_booking(@repair_booking).deliver_now if @repair_booking.valid?
  end

  private

  def permitted_attributes
    params.permit(:user_name, :phone, :description)
  end
end
