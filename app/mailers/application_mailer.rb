class ApplicationMailer < ActionMailer::Base
  include SendGrid

  default from: 'admin@imolodca.com'
  layout 'mailer'
end
