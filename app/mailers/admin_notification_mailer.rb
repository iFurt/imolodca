class AdminNotificationMailer < ApplicationMailer
  sendgrid_category :use_subject_lines

  def repair_booking(repair_booking)
    sendgrid_category "iMolodca"
    @repair_booking = repair_booking
    mail subject: "Запрос на бронирование ремонта", to: ENV['ADMINS'].split(',')
  end

  def repair_request(repair_request)
    sendgrid_category "iMolodca"
    @repair_request = repair_request
    mail subject: "Запрос на ремонт", to: ENV['ADMINS'].split(',')
  end

  def user_gadget_request(user_gadget_request)
    sendgrid_category "iMolodca"
    @user_gadget_request = user_gadget_request
    mail subject: "Запрос на продажу гаджета", to: ENV['ADMINS'].split(',')
  end

  def become_a_partner(name, contact)
    sendgrid_category "iMolodca"
    @name = name
    @contact = contact
    mail subject: "Запрос на партнерство", to: ENV['ADMINS'].split(',')
  end
end
