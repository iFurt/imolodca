$(function(){

  normalizeSlides = function() {
    var slide0 = $('.item0');
    var slide1 = $('.item1');
    var height0 = slide0.outerHeight();
    var height1 = slide1.outerHeight();

    if (height0 > height1) {
      slide1.css('height', height0 + 'px');
    } else {
      slide0.css('height', height1 + 'px');
    }
  }

  normalizeSlides();

  $( window ).resize(function() {
    normalizeSlides();
  });

  $('body').on('focus', '.carousel-inner', function() {
    var owl = $('#owl-carousel-one');
    owl.owlCarousel();
    owl.trigger('stop.owl.autoplay')
  });

  $('body').on('focusout', '.carousel-inner', function() {
    var owl = $('#owl-carousel-one');
    owl.owlCarousel();
    owl.trigger('play.owl.autoplay')
  });

  $('body').on('click', '.user-content-window input[type="submit"]', function() {
    var that = $(this);
    var name = that.data("master");

    var modal = $('#bookRepairModal #description').val("Я хочу вызвать мастера "+name)
  })

  $('body').on('click', '.navbar-toggle', function() {
    var navbar = $('.navbar');
    console.log('asd')
    if (navbar.hasClass("dark-nav")) {
      navbar.removeClass('dark-nav');
    } else {
      navbar.addClass('dark-nav');
    }
  })

  $('.links').click(function (event) {
    event = event || window.event;
    var target = event.target || event.srcElement,
      link = target.src ? target.parentNode : target,
      options = {index: link, event: event, continuous: true},
      links = this.getElementsByTagName('a');
    blueimp.Gallery(links, options);
  });

  var countdown = function() {

    // Get todays date and time
    var now = new Date().getTime();

    var deadline = new Date('2019-07-31').getTime();

    // Find the distance between now an the count down date
    var distance = deadline - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    days = days < 10 ? "0"+days : ""+days;
    hours = hours < 10 ? "0"+hours : ""+hours;
    minutes = minutes < 10 ? "0"+minutes : ""+minutes;
    seconds= seconds < 10 ? "0"+seconds : ""+seconds;

    // Display the result in the element with id="demo"
    document.getElementById("timer").innerHTML = hours + ":"
    + minutes + ":" + seconds;

    // If the count down is finished, write some text
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("timer").innerHTML = "ИСТЕКЛО";
    }
  };

  var x = setInterval(countdown, 1000);

  countdown();

  $('#owl-carousel-one').owlCarousel({
    loop: true,
    nav: false,
    items: 1,
    autoplay: true,
    autoplayTimeout: 8000
  })

  $('#owl-carousel1').owlCarousel({
      loop:false,
      nav: true,
      margin:10,
      navText: ['<span class="glyphicon glyphicon-menu-left"></span>','<span class="glyphicon glyphicon-menu-right"></span>'],
      responsive:{
          0:{
              items:1
          },
          747:{
              items:2
          },
          1100:{
              items:3
          }
      }
  })

  $('#owl-carousel2').owlCarousel({
      loop:true,
      margin:50,
      dots:false,
      responsive:{
          0:{
              items:2
          },
          747:{
              items:3
          },
          1100:{
              items:4
          }
      }
  })

  var owl3 = $('#owl-carousel3');
  owl3.owlCarousel({
      loop:true,
      margin:10,
      dots:false,
      navText: ['<span class="glyphicon glyphicon-menu-left"></span>','<span class="glyphicon glyphicon-menu-right"></span>'],
      items:1,
      autoHeight:true,
      responsive:{
          0:{
            nav: false
          },
          747:{
            nav: true
          }
      }
  });


})
