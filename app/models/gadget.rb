class Gadget < ApplicationRecord
  has_many :gadget_models
  has_many :repair_costs, through: :gadget_models
end
