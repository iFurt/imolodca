class UserGadget < ApplicationRecord
  mount_uploaders :images, UserGadgetUploader

  def image
    images.first.url
  end
end
