class Promo < ApplicationRecord
  mount_uploader :image, PromoUploader

  class << self
    def active
      find_by(active: true)
    end
  end
end
