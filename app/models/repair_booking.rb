class RepairBooking < ApplicationRecord
  validates :user_name, :phone, presence: true
end
