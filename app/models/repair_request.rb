class RepairRequest < ApplicationRecord
  validates :user_name, :phone, presence: true
end
