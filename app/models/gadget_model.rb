class GadgetModel < ApplicationRecord
  belongs_to :gadget
  has_many :repair_costs
end
