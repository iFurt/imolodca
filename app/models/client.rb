class Client < ApplicationRecord
  enum client_type: { ppl: 0, cmp: 1 }

  mount_uploader :image, ClientUploader
end
