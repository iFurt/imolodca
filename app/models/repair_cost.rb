class RepairCost < ApplicationRecord
  belongs_to :gadget_model
  has_one :gadget, through: :gadget_model

  enum price_type: { price_normal: 0, price_from: 1, price_to: 2 }

  default_scope -> { order(:id) }
end
