module ApplicationHelper
  def img_stars(count = 1)
    concat(*(image_tag 'star.png' * count))
  end

  def price_with_type(repair_cost)
    if repair_cost.promo_price.present? && repair_cost.promo_price >= 0
      return "От <strike>#{repair_cost.price.to_f.round(0)}</strike> #{repair_cost.promo_price.to_f.round(0)}".html_safe if repair_cost.price_from?
      return "До <strike>#{repair_cost.price.to_f.round(0)}</strike> #{repair_cost.promo_price.to_f.round(0)}".html_safe if repair_cost.price_to?
      "<strike>#{repair_cost.price.to_f.round(0)}</strike> #{repair_cost.promo_price.to_f.round(0)}".html_safe
    else
      return "От #{repair_cost.price.to_f.round(0)}" if repair_cost.price_from?
      return "До #{repair_cost.price.to_f.round(0)}" if repair_cost.price_to?
      repair_cost.price.to_f.round(0)
    end
  end
end
