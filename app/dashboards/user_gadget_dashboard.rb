require "administrate/base_dashboard"

class UserGadgetDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    name: Field::String,
    description: Field::Text,
    phone: Field::String,
    email: Field::String,
    rating: Field::Number,
    images: ImagesField,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :name,
    :description,
    :phone,
    :images
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :name,
    :description,
    :phone,
    :email,
    :rating,
    :images,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :name,
    :description,
    :phone,
    :email,
    :rating,
    :images,
  ].freeze

  # Overwrite this method to customize how user gadgets are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(user_gadget)
    "Гаджет ##{user_gadget.id} (#{user_gadget.name})"
  end

  def permitted_attributes
    super + [{ images: [] }]
  end
end
