require "administrate/base_dashboard"

class MasterDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    name: Field::String,
    phone: Field::String,
    email: Field::String,
    experience: Field::String,
    repairs_done: Field::Text,
    image: ImageField,
    marker_img: Field::String,
    rating: Field::Number.with_options(decimals: 2),
    lng: Field::Number.with_options(decimals: 2),
    lat: Field::Number.with_options(decimals: 2),
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :name,
    :phone,
    :email,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :name,
    :phone,
    :email,
    :experience,
    :repairs_done,
    :image,
    :rating,
    :lng,
    :lat,
    :marker_img,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :name,
    :phone,
    :email,
    :experience,
    :repairs_done,
    :image,
    :rating,
    :lng,
    :lat,
    :marker_img,
  ].freeze

  # Overwrite this method to customize how masters are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(master)
    "Мастер #{master.name} (#{master.email})"
  end
end
