require "administrate/base_dashboard"

class RepairCostDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    gadget_model: Field::BelongsTo,
    gadget: Field::HasOne,
    id: Field::Number,
    name: Field::String,
    price: Field::Number.with_options(decimals: 2),
    promo_price: Field::Number.with_options(decimals: 2),
    price_type: EnumField.with_options(collection: RepairCost.price_types.keys),
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :name,
    :gadget_model,
    :gadget,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :name,
    :price,
    :promo_price,
    :price_type,
    :gadget_model,
    :gadget,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :name,
    :price,
    :promo_price,
    :price_type,
    :gadget_model,
    :gadget,
  ].freeze

  # Overwrite this method to customize how repair costs are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(repair_cost)
    repair_cost.name
  end
end
