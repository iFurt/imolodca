Rails.application.routes.draw do

  namespace :admin do
    resources :gadgets
    resources :gadget_models
    resources :repair_costs
    resources :masters
    resources :user_gadgets
    resources :repair_bookings
    resources :repair_requests
    resources :subscriptions
    resources :user_gadget_requests
    resources :feedbacks
    resources :clients
    resources :promos
    resources :admin_identificators

    root to: "gadgets#index"
  end

  resources :repair_bookings, only: :create
  resources :repair_requests, only: :create
  resources :subscriptions, only: :create
  resources :user_gadget_requests, only: :create

  post :become_a_partner, controller: :welcome
  root 'welcome#index'
end
