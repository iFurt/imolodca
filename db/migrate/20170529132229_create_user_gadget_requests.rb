class CreateUserGadgetRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :user_gadget_requests do |t|
      t.string :phone

      t.timestamps
    end
  end
end
