class CreateMasters < ActiveRecord::Migration[5.0]
  def change
    create_table :masters do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :experience
      t.string :repairs_done
      t.string :phone
      t.string :image
      t.float :rating
      t.float :lng
      t.float :lat

      t.timestamps
    end
  end
end
