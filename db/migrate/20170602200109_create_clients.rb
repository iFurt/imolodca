class CreateClients < ActiveRecord::Migration[5.0]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :profession
      t.integer :client_type, default: 0
      t.string :image

      t.timestamps
    end
  end
end
