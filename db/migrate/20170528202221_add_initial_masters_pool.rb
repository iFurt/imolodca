class AddInitialMastersPool < ActiveRecord::Migration[5.0]
  def change
    Master.create(name: 'Володя', phone: '+380', email: 'no@email.atall', experience: '10 лет',
      repairs_done: 'Универсальный мастер. Особые направления - пайка touch id, ремонт ноутбуков,замена матриц на macbook и imac,пайка сред. сложности',
      rating: 5, lng: 30.637045, lat: 50.405225)

    Master.create(name: 'Алексей', phone: '+380', email: 'no@email.atall', experience: '10 лет',
      repairs_done: 'Ремонт гаджетов любой сложности, особое направление - пайка высшей сложности устройств apple',
      rating: 5, lng: 30.652170, lat: 50.402922)

    Master.create(name: 'Володя', phone: '+380', email: 'no@email.atall', experience: '10 лет',
      repairs_done: 'Универсальный мастер. Особое направление - ремонт Android, пайка легкой сложности.',
      rating: 5, lng: 30.528066, lat: 50.435064)

    Master.create(name: 'Александр', phone: '+380', email: 'no@email.atall', experience: '10 лет',
      repairs_done: 'Универсальный мастер. Индивидуальное направление; замена стекол на android, обновление, смена ПО apple/android, пайка легкой сложности',
      rating: 5, lng: 30.521156, lat: 50.444951)

    Master.create(name: 'Влад', phone: '+380', email: 'no@email.atall', experience: '7 лет',
      repairs_done: 'Индивидуальное направление; ремонт apple, быстрый модульный ремонт iphone/ipad, пайка легкой сложности',
      rating: 5, lng: 30.547683, lat: 50.434210)

    Master.create(name: 'Богдан', phone: '+380', email: 'no@email.atall', experience: '7 лет',
      repairs_done: 'Индивидуальное направление; ремонт apple, быстрый модульный ремонт iphone/ipad, пайка легкой сложности',
      rating: 5, lng: 30.618928, lat: 50.416250)

    Master.create(name: 'Никита', phone: '+380', email: 'no@email.atall', experience: '10 лет',
      repairs_done: '',
      rating: 5, lng: 0, lat: 0)

    Master.create(name: 'Женя', phone: '+380', email: 'no@email.atall', experience: '10 лет',
      repairs_done: '',
      rating: 5, lng: 0, lat: 0)

    Master.create(name: 'Дмитрий', phone: '+380', email: 'no@email.atall', experience: '10 лет',
      repairs_done: '',
      rating: 5, lng: 0, lat: 0)
  end
end
