class AddPromoPriceToRepairCosts < ActiveRecord::Migration[5.0]
  def change
    add_column :repair_costs, :promo_price, :integer
  end
end
