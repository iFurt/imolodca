class CreatePromos < ActiveRecord::Migration[5.0]
  def change
    create_table :promos do |t|
      t.string :name
      t.string :image
      t.boolean :active

      t.timestamps
    end
  end
end
