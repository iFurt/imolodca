class CreateGadgetModels < ActiveRecord::Migration[5.0]
  def change
    create_table :gadget_models do |t|
      t.string :name
      t.belongs_to :gadget

      t.timestamps
    end
  end
end
