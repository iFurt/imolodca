class CreateRepairCosts < ActiveRecord::Migration[5.0]
  def change
    create_table :repair_costs do |t|
      t.string :name
      t.float :price
      t.integer :price_type, default: 0
      t.belongs_to :gadget_model

      t.timestamps
    end
  end
end
