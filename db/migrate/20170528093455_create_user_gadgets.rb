class CreateUserGadgets < ActiveRecord::Migration[5.0]
  def change
    create_table :user_gadgets do |t|
      t.string :name
      t.text :description
      t.string :phone
      t.string :email
      t.integer :rating
      t.json :images

      t.timestamps
    end
  end
end
