class CreateAdminIdentificators < ActiveRecord::Migration[5.0]
  def change
    create_table :admin_identificators do |t|
      t.string :name
      t.string :value
      t.text :meta

      t.timestamps
    end
  end
end
