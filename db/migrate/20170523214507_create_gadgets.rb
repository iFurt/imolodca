class CreateGadgets < ActiveRecord::Migration[5.0]
  def change
    create_table :gadgets do |t|
      t.string :name
      t.string :canonical_name

      t.timestamps
    end
  end
end
