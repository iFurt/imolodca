class AddMapParamsToMasters < ActiveRecord::Migration[5.0]
  def change
    add_column :masters, :radius_color, :string, default: '#0000FF'
    add_column :masters, :marker_img, :string
  end
end
