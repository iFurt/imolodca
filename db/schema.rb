# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171014183259) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_identificators", force: :cascade do |t|
    t.string   "name"
    t.string   "value"
    t.text     "meta"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "clients", force: :cascade do |t|
    t.string   "name"
    t.string   "profession"
    t.integer  "client_type", default: 0
    t.string   "image"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "feedbacks", force: :cascade do |t|
    t.string   "name"
    t.text     "comment"
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gadget_models", force: :cascade do |t|
    t.string   "name"
    t.integer  "gadget_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gadget_id"], name: "index_gadget_models_on_gadget_id", using: :btree
  end

  create_table "gadgets", force: :cascade do |t|
    t.string   "name"
    t.string   "canonical_name"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "masters", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "experience"
    t.string   "repairs_done"
    t.string   "image"
    t.float    "rating"
    t.float    "lng"
    t.float    "lat"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "radius_color", default: "#0000FF"
    t.string   "marker_img"
  end

  create_table "promos", force: :cascade do |t|
    t.string   "name"
    t.string   "image"
    t.boolean  "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "repair_bookings", force: :cascade do |t|
    t.string   "user_name"
    t.string   "phone"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "repair_costs", force: :cascade do |t|
    t.string   "name"
    t.float    "price"
    t.integer  "price_type",      default: 0
    t.integer  "gadget_model_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "promo_price"
    t.index ["gadget_model_id"], name: "index_repair_costs_on_gadget_model_id", using: :btree
  end

  create_table "repair_requests", force: :cascade do |t|
    t.string   "user_name"
    t.string   "phone"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "subscriptions", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_gadget_requests", force: :cascade do |t|
    t.string   "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_gadgets", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "phone"
    t.string   "email"
    t.integer  "rating"
    t.json     "images"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

end
